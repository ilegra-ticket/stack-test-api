/**
 * @author daCOSTA, Giovani Hoff <giovani.costa@ilegra.com>
 */

var chakram = require('chakram'),
    expect = chakram.expect;

const cardConfig = require("../typeScript/pages/common.js");
const cardTicket = new cardConfig.TicketPageObject();

var data = {
  accessToken:cardTicket.accessToken,
  alternativeAccessToken:'none',
  alternativeEmail:cardTicket.alternativeEmail,
  alternativeUserID:'*',
  authenticationPhone:cardTicket.authenticationPhone,
  appToken:cardTicket.appToken,
  birth:cardTicket.birth,
  cpf:cardTicket.cpf,
  domainI:cardTicket.domainI,
  domainII:cardTicket.domainII,
  email:cardTicket.email,
  endpointAuthenticate:cardTicket.endpointAuthenticate,
  endpointCard:cardTicket.endpointCard,
  endpointDelete:cardTicket.endpointDelete,
  endpointRegister:cardTicket.endpointRegister,
  endpointPutUser:cardTicket.endpointPutUser,
  facebookName:cardTicket.facebookName,
  facebookPassword:cardTicket.facebookPassword,
  facebookUser:cardTicket.facebookUser,
  foodCard:cardTicket.foodCard,
  invalidEmail:cardTicket.invalidEmail,
  name:cardTicket.name,
  password:cardTicket.password,
  phone:cardTicket.phone,
  registration:cardTicket.registration,
  unregisteredEmail:cardTicket.unregisteredEmail,
  userID:'*',
  userToken:cardTicket.userToken,
  waitTime:30000
};

var url = {
  authenticateUser:data.domainI + data.endpointAuthenticate,
  consultCard:data.domainII + data.endpointCard + data.userID + '/cartoes/simplificados',
  deleteCard:data.domainI + '/usuarios/v3/' + data.userID + '/cartoes',
  deleteUser:data.domainI + data.endpointDelete + data.userID,
  registerCard:data.domainII + data.endpointCard + data.userID + '/cartoes',
  registerUser:data.domainI + data.endpointRegister,
  reviewUser:data.domainI + data.endpointPutUser,
  updateUser:data.domainI + '/usuarios/' + data.userID + '/autenticacao'
};

describe("Ticket - API - Cadastrar Usuário [POST: " + url.registerUser + "]", function() {
  it("Ticket - API - Cadastrar Usuário [" + data.email + "] - Validar mensagem 200", async function () {
    var body = {
      cpf:data.cpf,
      token:data.appToken,
      password:data.password,
      cellPhone:data.phone,
      name:data.name,
      matricula:data.registration,
      nascimento:data.birth,
      email:data.email
    };
    var response = await chakram.post(url.registerUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.value.email).to.equal(data.email);
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Autenticar [POST: " + url.authenticateUser + "]", function() {
  it("Ticket - API - Autenticar [" + data.email + "] - Validar mensagem 200", async function () {
    var body = {
      email:data.email,
      token:data.userToken
    };
    var response = await chakram.post(url.authenticateUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    data.accessToken = response.body.access_token;
    data.userID = response.body.id_user;
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Cadastrar Cartões [POST: " + url.registerCard + "]", function() {
  it("Ticket - API - Cadastrar Cartão ALIMENTAÇÃO [" + data.foodCard + "] para [" + data.email + "] - Validar mensagem 200", async function () {
    var body = {
      number:data.foodCard,
      nickname:'CARTÃO ALIMENTAÇÃO'
    }
    var url = data.domainII + data.endpointCard + data.userID + '/cartoes';
    var auth = 'Bearer ' + data.accessToken;
    var response = await chakram.post(url, body, {timeout:data.waitTime, headers: {'content-type': 'application/json', 'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    expect(response.body.message).to.equal('Ok');
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Consultar Cartões [GET: " + url.consultCard + "]", function() {
  it("Ticket - API - Consultar Cartão ALIMENTAÇÃO [" + data.foodCard + "] para [" + data.email + "] - Validar mensagem 200", async function () {
    var url = data.domainII + data.endpointCard + data.userID + '/cartoes/simplificados';
    var auth = 'Bearer ' + data.accessToken;
    var response = await chakram.get(url, {timeout:data.waitTime, headers: {'content-type': 'application/json', 'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    expect(response.body.message).to.equal('Ok');
    expect(response.body.value[0].balance.number).to.equal(data.foodCard);
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Remover Cartões [DELETE: " + url.deleteCard + "]", function() {
  it("Ticket - API - Deletar Cartão ALIMENTAÇÃO [" + data.foodCard + "] para [" + data.email + "] - Validar mensagem 200", async function () {
    var url = data.domainI + '/usuarios/v3/' + data.userID + '/cartoes';
    var auth = 'Bearer ' + data.accessToken;
    var response = await chakram.delete(url, null, {timeout:data.waitTime, headers: {'content-type': 'application/json', 'authorization': auth, 'number': data.foodCard}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Remover Usuários [DELETE: " + url.deleteUser + "]", function() {
  it("Ticket - API - Deletar Usuário [" + data.email + "] - Validar mensagem 200 - Usuário Cadastrado", async function () {
    var url = data.domainI + data.endpointDelete + data.userID;
    var auth = 'Bearer ' + data.accessToken;
    var response = await chakram.delete(url, null, {timeout:data.waitTime, headers: {'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    return chakram.wait();
  }).timeout(data.waitTime);
});
