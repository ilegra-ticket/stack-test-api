/**
 * @author daCOSTA, Giovani Hoff <giovani.costa@ilegra.com>
 */

var chakram = require('chakram'),
    expect = chakram.expect;

const authenticationConfig = require("../typeScript/pages/common.js");
const authenticationTicket = new authenticationConfig.TicketPageObject();

var data = {
  accessToken:authenticationTicket.accessToken,
  alternativeAccessToken:'none',
  alternativeEmail:authenticationTicket.alternativeEmail,
  alternativeUserID:'*',
  authenticationPhone:authenticationTicket.authenticationPhone,
  appToken:authenticationTicket.appToken,
  birth:authenticationTicket.birth,
  cpf:authenticationTicket.cpf,
  domainI:authenticationTicket.domainI,
  domainII:authenticationTicket.domainII,
  email:authenticationTicket.email,
  endpointAuthenticate:authenticationTicket.endpointAuthenticate,
  endpointCard:authenticationTicket.endpointCard,
  endpointDelete:authenticationTicket.endpointDelete,
  endpointRegister:authenticationTicket.endpointRegister,
  endpointPutUser:authenticationTicket.endpointPutUser,
  facebookName:authenticationTicket.facebookName,
  facebookPassword:authenticationTicket.facebookPassword,
  facebookUser:authenticationTicket.facebookUser,
  foodCard:authenticationTicket.foodCard,
  invalidEmail:authenticationTicket.invalidEmail,
  name:authenticationTicket.name,
  password:authenticationTicket.password,
  phone:authenticationTicket.phone,
  registration:authenticationTicket.registration,
  unregisteredEmail:authenticationTicket.unregisteredEmail,
  userID:'*',
  userToken:authenticationTicket.userToken,
  waitTime:30000
};

var url = {
  authenticateUser:data.domainI + data.endpointAuthenticate,
  consultCard:data.domainII + data.endpointCard + data.userID + '/cartoes/simplificados',
  deleteCard:data.domainI + '/usuarios/v3/' + data.userID + '/cartoes',
  deleteUser:data.domainI + data.endpointDelete + data.userID,
  registerCard:data.domainII + data.endpointCard + data.userID + '/cartoes',
  registerUser:data.domainI + data.endpointRegister,
  reviewUser:data.domainI + data.endpointPutUser,
  updateUser:data.domainI + '/usuarios/' + data.userID + '/autenticacao'
};

describe("Ticket - API - Cadastrar Usuários [POST: " + url.registerUser + "]", function() {
  it("Ticket - API - Cadastrar Usuário [" + data.email + "] - Validar mensagem 200", async function () {
    var body = {
      cpf:data.cpf,
      token:data.appToken,
      password:data.password,
      cellPhone:data.phone,
      name:data.name,
      matricula:data.registration,
      nascimento:data.birth,
      email:data.email
    };
    var response = await chakram.post(url.registerUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.value.email).to.equal(data.email);
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Cadastrar Usuário [" + data.alternativeEmail + "] - Validar mensagem 200", async function () {
    var body = {
      password:data.password,
      name:data.name,
      email:data.alternativeEmail
    };
    var response = await chakram.post(url.registerUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.value.email).to.equal(data.alternativeEmail);
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Cadastrar Usuário [" + data.email + "] - Validar mensagem 400", async function () {
    var body = {
      cpf:data.cpf,
      token:data.appToken,
      password:data.password,
      cellPhone:data.phone,
      name:data.name,
      email:data.email
    };
    var response = await chakram.post(url.registerUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(400);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    expect(response.body.message).to.equal('Usuário já cadastrado');
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Cadastrar Usuário [" + data.alternativeEmail + "] - Validar mensagem 400", async function () {
    var body = {
      password:data.password,
      name:data.name,
      email:data.alternativeEmail
    };
    var response = await chakram.post(url.registerUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(400);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    expect(response.body.message).to.equal('Usuário já cadastrado');
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Autenticar [POST: " + url.authenticateUser + "]", function() {
  it("Ticket - API - Autenticar [" + data.email + "] - Validar mensagem 200", async function () {
    var body = {
      email:data.email,
      token:data.userToken
    };
    var response = await chakram.post(url.authenticateUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    data.accessToken = response.body.access_token;
    data.userID = response.body.id_user;
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Autenticar [" + data.alternativeEmail + "] - Validar mensagem 200", async function () {
    var body = {
      email:data.alternativeEmail,
      password:data.password
    };
    var response = await chakram.post(url.authenticateUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    data.alternativeAccessToken = response.body.access_token;
    data.alternativeUserID = response.body.id_user;
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Autenticar [" + data.email + "] - Validar mensagem 400", async function () {
    var body = {
      email:data.unregisteredEmail,
      token:data.userToken
    };
    var response = await chakram.post(url.authenticateUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(400);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    expect(response.body.message).to.equal('Parece que você não tem uma conta Ticket com este e-mail.');
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Autenticar [" + data.alternativeEmail + "] - Validar mensagem 400", async function () {
    var body = {
      email:data.alternativeEmail,
      password:data.password + 'XYZ'
    };
    var response = await chakram.post(url.authenticateUser, body, {timeout:data.waitTime, headers: {'content-type': 'application/json'}});
    expect(response).to.have.status(400);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    expect(response.body.message).to.equal('E-mail ou senha inválidos.');
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Atualizar Dados Cadastrais [POST: " + url.updateUser + "]", function() {
  it("Ticket - API - Atualizar Dados Cadastrais para [" + data.email + "] - Validar mensagem 200", async function () {
    var body = {
      email:data.email,
      name:data.name + ' ATUALIZADO',
      password:data.password
    }
    var auth = 'Bearer ' + data.accessToken;
    var url = data.domainI + '/usuarios/' + data.userID + '/autenticacao';
    var response = await chakram.post(url, body, {timeout:data.waitTime, headers: {'content-type': 'application/json', 'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    expect(response.body.message).to.equal('Correção de dados executado com sucesso.');
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Atualizar Dados Cadastrais para [" + data.email + "] - Validar mensagem 400", async function () {
    var body = {
      email:data.email,
      name:data.name + 'ERRADO',
      password:data.password
    }
    var auth = 'Bearer ' + data.accessToken;
    var url = data.domainI + '/usuarios/' + data.userID + '/autenticacao';
    var response = await chakram.post(url, body, {timeout:data.waitTime, headers: {'content-type': 'application/json', 'authorization': auth}});
    expect(response).to.have.status(400);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    expect(response.body.message).to.equal('Este e-mail já está sendo utilizado por outro usuário.');
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Atualizar Dados Cadastrais para [" + data.alternativeEmail + "] - Validar mensagem 400", async function () {
    var body = {
      email:data.alternativeEmail,
      name:data.name + 'ERRADO',
      password:data.password
    }
    var auth = 'Bearer ' + data.alternativeAccessToken;
    var url = data.domainI + '/usuarios/' + data.alternativeUserID + '/autenticacao';
    var response = await chakram.post(url, body, {timeout:data.waitTime, headers: {'content-type': 'application/json', 'authorization': auth}});
    expect(response).to.have.status(400);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    expect(response.body.message).to.equal('Atualização do cadastro já realizada.');
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Revisar Dados Cadastrais [PUT: " + url.reviewUser + "]", function() {
  it("Ticket - API - Revisar Dados Cadastrais para [" + data.alternativeEmail + "] - Validar mensagem 200", async function () {
    var body = {
      id:data.alternativeUserID,
      email:data.alternativeEmail,
      name:data.name + 'REVISADO',
      password:data.password
    }
    var auth = 'Bearer ' + data.alternativeAccessToken;
    var response = await chakram.put(url.reviewUser, body, {timeout:data.waitTime, headers: {'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    return chakram.wait();
  }).timeout(data.waitTime);
});

describe("Ticket - API - Remover Usuários [DELETE: " + url.deleteUser + "]", function() {
  it("Ticket - API - Deletar Usuário [" + data.email + "] - Validar mensagem 200 - Usuário Cadastrado", async function () {
    var url = data.domainI + data.endpointDelete + data.userID;
    var auth = 'Bearer ' + data.accessToken;
    var response = await chakram.delete(url, null, {timeout:data.waitTime, headers: {'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Deletar Usuário [" + data.alternativeEmail + "] - Validar mensagem 200 - Usuário Cadastrado", async function () {
    var url = data.domainI + data.endpointDelete + data.alternativeUserID;
    var auth = 'Bearer ' + data.alternativeAccessToken;
    var response = await chakram.delete(url, null, {timeout:data.waitTime, headers: {'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(true);
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Deletar Usuário [" + data.email + "] - Validar mensagem 200 - Usuário NÃO Cadastrado", async function () {
    var url = data.domainI + data.endpointDelete + data.userID;
    var auth = 'Bearer ' + data.accessToken;
    var response = await chakram.delete(url, null, {timeout:data.waitTime, headers: {'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    return chakram.wait();
  }).timeout(data.waitTime);
  it("Ticket - API - Deletar Usuário [" + data.alternativeEmail + "] - Validar mensagem 200 - Usuário NÃO Cadastrado", async function () {
    var url = data.domainI + data.endpointDelete + data.alternativeUserID;
    var auth = 'Bearer ' + data.alternativeAccessToken;
    var response = await chakram.delete(url, null, {timeout:data.waitTime, headers: {'authorization': auth}});
    expect(response).to.have.status(200);
    expect(response).not.to.have.header('non-existing-header');
    expect(response.body.success).to.equal(false);
    return chakram.wait();
  }).timeout(data.waitTime);
});
