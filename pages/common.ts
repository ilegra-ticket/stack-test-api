/**
 * @author daCOSTA, Giovani Hoff <giovani.costa@ilegra.com>
 */

 /**
  * '', '_qa', '_hom'
  */
const ENVIRONMENT = '_qa';

export class TicketPageObject {
  /**
   * problems with no back-end pattern
   */
  private selectDeleteEndPoint = (type: any): string => {
    if(type === 'dev') {
      return '/api/usuarios/';
    } else {
      return '/api/Usuarios/';
    }
  }
  private selectCardEndPoint = (type: any): string => {
    if(type === 'dev') {
      return '/v1/usuarios/';
    } else {
      return 'o/v1/usuarios/';
    }
  }

  public environment: string = ENVIRONMENT;
  private selectEnvironmentType = (type: any): string => {
    if(this.environment === '_hom') {
      return 'hom';
    } else if (this.environment === '_qa') {
      return 'dev';
    } else return '';
  }
  public type: string = this.selectEnvironmentType(this.environment);

  public accessToken: string = 'none';
  public alternativeEmail: string = 'giovanihoff@gmail.com';
  public appToken: string = '1096170830453665|RJ2ohQTpcIk0CRb0rAd8CJAK2u0';
  public authenticationPhone: string = '+5551985783092';
  public birth: string = '02/09/1945';
  public cpf: string = '38485932897';
  public domainI = 'https://api.ticket.com.br/mobile' + this.environment;
  public domainII = 'https://api.ticket.com.br/app/mobile' + this.environment;
  public email: string = 'giovani.costa@ilegra.com';
  public endpointAuthenticate: string = '/usuarios/v3/autenticar';
  public endpointCard: string = this.selectCardEndPoint(this.type);
  public endpointDelete: string = this.selectDeleteEndPoint(this.type);
  public endpointPutUser: string = '/usuarios/v3/';
  public endpointRegister: string = '/usuarios/v3.1';
  public facebookName: string = 'Giovani Hoff';
  public facebookPassword: string = 'test.123';
  public facebookUser: string = 'giovani.hoff.12';
  public foodCard = '6026515023466000';
  public invalidEmail = 'Giovani Hoff <giovanihoff@gmail.com>';
  public name: string = 'Fulano Beltrano de T@l01.';
  public password: string = 'test123';
  public phone = '(51)98578-3092';
  public registration = '1234567890';
  public unregisteredEmail: string = 'giovanihoff@gmail.com.br';
  public userId: string = 'none';
  public userToken: string = 'EAAPk9ij2W6EBAJJfM58hHkYfn1ZC1ZBjtlXRVn3wIZCcvh0KSvwGVVkkqM0gri3KUgNs3FgEO8YNu1jmj0XmPYm4dibUZABskmygrw8ZBUS9CE1nlsQozPxaxw0J8pQzZClqpIzDNzbmgHfUWGOCLq5Mwl5hc9mLYZD';
  public waitTime: number = 45000;
}
